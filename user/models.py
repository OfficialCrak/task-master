from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    birth_date = models.DateField(null=True, blank=True, verbose_name='Дата рождения')
    patronymic = models.CharField(max_length=50, null=True, blank=True, verbose_name='Отчество')
    phone = models.CharField(max_length=15, null=True, blank=True, verbose_name='Телефон')
    role = models.ForeignKey('Role', null=True, blank=True, on_delete=models.PROTECT, verbose_name='Роль', default=2)
    job = models.ForeignKey('Job_title', null=True, blank=True, on_delete=models.PROTECT, verbose_name='Должность')
    company = models.ForeignKey('Company', null=True, blank=True, on_delete=models.CASCADE, verbose_name='Компания')
    is_approved = models.BooleanField(default=False, verbose_name='Подтвержден')


class Role(models.Model):
    title = models.CharField(max_length=25, verbose_name='Название роли')

    class Meta:
        verbose_name_plural = 'Роли'
        verbose_name = 'Роль'

    def __str__(self):
        return self.title


class Job_title(models.Model):
    title = models.CharField(max_length=50, verbose_name='Должность')

    class Meta:
        verbose_name_plural = 'Должности'
        verbose_name = 'Должность'

    def __str__(self):
        return self.title


class Company(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название компании')
    ip = models.CharField(max_length=255, verbose_name='ИП')
    phone = models.CharField(max_length=15, verbose_name='Телефон', null=True, blank=True)
    address = models.CharField(max_length=255, verbose_name='Адрес', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Компании'
        verbose_name = 'Компания'

    def __str__(self):
        return self.title
