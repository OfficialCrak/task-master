import requests
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from .views import UserRegisterationAPIView, UserLoginAPIView, APIUser, DetailUserView, CompanyInfo, JobInfo, LogoutView, \
    confirm_registration, confirmation_success, ChangeJobView
from rest_framework_simplejwt.views import TokenRefreshView


urlpatterns = [
    # post views
    path('list/', APIUser.as_view(), name='fulllist-user'),
    path('company/', CompanyInfo.as_view(), name='list-company'),
    path('job/', JobInfo.as_view(), name='list-job'),
    path('profile/', DetailUserView.as_view(), name='detail-user'),
    path('register/', UserRegisterationAPIView.as_view(), name='create-user'),
    path('login/', UserLoginAPIView.as_view(), name='login-user'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('logout/', LogoutView.as_view(), name='logout-user'),
    path('confirm-registration/<str:token>/', confirm_registration, name='confirm-registration'),
    path('confirmation-success/', confirmation_success, name='confirmation-success-page'),
    path('<int:pk>/change-job/', ChangeJobView.as_view()),
]
