import smtplib
from django.core.mail.backends.base import BaseEmailBackend
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class SMTPEmailBackend(BaseEmailBackend):
    def send_messages(self, email_messages):
        success_count = 0
        for message in email_messages:
            msg = MIMEMultipart()
            msg['From'] = 'chirjeveg@gmail.com'
            msg['To'] = message.to[0]  # Принимаем только первый адрес получателя
            msg['Subject'] = message.subject
            msg.attach(MIMEText(message.body, 'html'))

            try:
                with smtplib.SMTP('smtp.gmail.com', 587) as server:
                    server.starttls()
                    server.login('chirjeveg@gmail.com', 'zokhngohtxydrotq')
                    server.send_message(msg)
                    success_count += 1
            except Exception as e:
                print(f"Failed to send email: {e}")

        return success_count
