from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
from django.core.exceptions import MultipleObjectsReturned

UserModel = get_user_model()


class EmailUsernameAuthenticationBackend(ModelBackend):
    def authenticate(self, request, username=None, email=None, password=None, **kwargs):
        if not email:  # Если email не предоставлен, не пытаемся аутентифицировать
            return None
        try:
            user = UserModel.objects.get(username=username, email=email)
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
        except UserModel.DoesNotExist:
            return None


class UsernameOnlyBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        if not request or not request.path.startswith('/admin/'):  # Используем только для админ-панели
            return None
        try:
            user = UserModel.objects.get(username=username)
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
        except UserModel.DoesNotExist:
            return None