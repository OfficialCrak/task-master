from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser, Role, Job_title, Company


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ['email', 'username', 'first_name', 'last_name', 'role', 'job', 'birth_date', 'company']
    add_fieldsets = (
        *UserAdmin.add_fieldsets,
        (
            'Custom fields',
            {
                'fields': (
                    'email',
                    'company',
                )
            }
        )
    )
    fieldsets = (
        *UserAdmin.fieldsets,
        (
            'Custom fields',
            {
                'fields': (
                    'patronymic',
                    'phone',
                    'role',
                    'job',
                    'birth_date',
                    'company',
                    'is_approved',
                )
            }
        )
    )


class RoleAdmin(admin.ModelAdmin):
    list_display = ('title',)


class JobAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(Role, RoleAdmin)
admin.site.register(Job_title, JobAdmin)
admin.site.register(Company)
