import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from user.models import CustomUser
import os
from email.message import EmailMessage

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Send an image to a user via email'

    def add_arguments(self, parser):
        parser.add_argument('username', type=str, help='Username of the recipient')
        parser.add_argument('--image', type=str, help='Path to the image file')

    def handle(self, *args, **kwargs):
        username = kwargs['username']
        image_relative_path = kwargs['image']

        try:
            user = CustomUser.objects.get(username=username)
        except CustomUser.DoesNotExist:
            self.stdout.write(self.style.ERROR(f'User with username {username} does not exist'))
            return

        current_directory = os.path.dirname(__file__)
        image_path = os.path.join(current_directory, image_relative_path)

        if not os.path.exists(image_path):
            self.stdout.write(self.style.ERROR(f'Image file not found at path: {image_path}'))
            return

        html_message = f"""
        <html>
        <body>
            <p style="color: red; font-size: 18px;">Здравствуйте, {username}, а вот и ваш отчет. Зацените</p>
        </body>
        </html>
        """
        text = MIMEText(html_message, 'html')

        # Отправка изображения
        try:
            # Подготовка сообщения
            msg = MIMEMultipart()
            msg['From'] = 'chirjeveg@gmail.com'
            msg['To'] = user.email
            msg['Subject'] = 'Image from admin command Task-Master'

            # Чтение изображения
            with open(image_path, 'rb') as image_file:
                image_data = image_file.read()

            # Добавление текстовой части
            msg.attach(text)

            # Добавление изображения
            image = MIMEImage(image_data, name='pirt.jpg')
            msg.attach(image)

            # Отправка почты
            smtp_server = 'smtp.gmail.com'
            smtp_port = 587
            smtp_username = 'chirjeveg@gmail.com'
            smtp_password = 'zokhngohtxydrotq'

            with smtplib.SMTP(smtp_server, smtp_port) as server:
                server.starttls()
                server.login(smtp_username, smtp_password)
                server.sendmail(smtp_username, user.email, msg.as_string())

            self.stdout.write(self.style.SUCCESS(f'Image sent to {username} successfully'))
        except Exception as e:
            self.stdout.write(self.style.ERROR(f'Failed to send image: {str(e)}'))
