from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'My custom command'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('This is my custom command!'))
