import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.urls import reverse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import AccessToken
from django.contrib.auth import get_user_model

from task.permissions import IsDirectorOrAdmin
from .models import CustomUser, Company, Job_title
from .serializers import UserSerializers, ChangeJobSerializer, ListInfoUser, RegistrationSerializer, UserFullSerializers, UserLoginSerializer, CustomUserSerializer, CompanyTitleSerializers, JobSerializers, UserPutSerializers
from rest_framework.generics import RetrieveUpdateAPIView, CreateAPIView, ListAPIView, DestroyAPIView, \
    get_object_or_404, RetrieveUpdateDestroyAPIView, ListCreateAPIView, GenericAPIView, UpdateAPIView
from rest_framework.exceptions import PermissionDenied
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.exceptions import AuthenticationFailed
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags

User = get_user_model()


class ChangeJobView(UpdateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = ChangeJobSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        company_id = self.request.user.company_id
        return CustomUser.objects.filter(company_id=company_id)

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), pk=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def check_object_permissions(self, request, obj):
        super().check_object_permissions(request, obj)

        # Получаем ID должности, которую хочет назначить пользователь.
        requested_job_id = request.data.get('job')

        # Проверяем, является ли пользователь директором.
        if request.user.role_id == 4:
            # Директор может назначить любую должность.
            return
        else:
            # Получаем объект должности пользователя, который делает запрос.
            user_job = request.user.job

            # Проверяем, что пользователь не пытается назначить запрещенные должности.
            if requested_job_id in [5, 6, 14]:
                raise PermissionDenied('Вы не можете назначить эту должность.')

            # Проверяем, что пользователь не пытается изменить должность директора.
            if obj.role_id == 4:
                raise PermissionDenied('Вы не можете изменить должность директора.')

            # Проверяем, что пользователь с job=14.
            if user_job.id == 14:
                # Пользователь с job=14 не может изменять должность на job=5, 6, 14.
                if requested_job_id in [5, 6, 14]:
                    raise PermissionDenied('Вы не можете назначить эту должность.')


def create_confirmation_token(user):
    # Создаём AccessToken вместо RefreshToken
    token = AccessToken.for_user(user)
    # Добавляем кастомные данные в токен
    token['username'] = user.username
    token['is_confirmation_token'] = True
    return str(token)


class APIUser(ListAPIView):
    permission_classes = [permissions.IsAdminUser, ]
    queryset = CustomUser.objects.all()
    serializer_class = ListInfoUser


class APIDeleteUser(DestroyAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserFullSerializers
    queryset = CustomUser.objects.all()


class DetailUserView(RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserSerializers
    queryset = CustomUser.objects.all()

    def get_object(self):
        return self.request.user

    @swagger_auto_schema(request_body=UserPutSerializers)
    def put(self, request, *args, **kwargs):
        self.serializer_class = UserPutSerializers
        return self.update(request, *args, **kwargs)


class UserRegisterationAPIView(GenericAPIView):
    permission_classes = [permissions.AllowAny, ]
    serializer_class = RegistrationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        company = user.company
        director = CustomUser.objects.filter(company=company, role__id=4).first()

        if director:
            confirmation_token = create_confirmation_token(user)
            confirmation_url = f'http://localhost:3000/register-confirm/{confirmation_token}'

            html_content = f"""
                        <html>
                            <head>
                                <style type="text/css">
                                    .email-body {{
                                        font-family: 'Arial', sans-serif;
                                    }}
                                    .email-content {{
                                        color: #333333;
                                        font-size: 16px;
                                    }}
                                    .email-link {{
                                        color: #1a82e2;
                                        font-weight: bold;
                                        font-size: 18px;
                                        text-decoration: none;
                                    }}
                                </style>
                            </head>
                            <body class="email-body">
                                <div class="email-content">
                                    Пользователь {user.username}, {user.email} запросил регистрацию в вашей компании {company.title}.
                                    <p>Пожалуйста, <a href="{confirmation_url}" class="email-link">перейдите по этой ссылке</a> для подтверждения регистрации.</p>
                                </div>
                            </body>
                        </html>
                        """

            self.send_html_email(
                subject='Подтверждение регистрации нового пользователя',
                html_content=html_content,
                from_email='chirjeveg@gmail.com',
                to_email=director.email,
            )

        data = {
            'user_id': user.id,
            'username': user.username,
            'email': user.email,
        }
        return Response(data, status=status.HTTP_201_CREATED)

    def send_html_email(self, subject, html_content, from_email, to_email):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = from_email
        msg['To'] = to_email

        part2 = MIMEText(html_content, 'html')
        msg.attach(part2)

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(from_email, 'zokhngohtxydrotq')
        server.sendmail(from_email, to_email, msg.as_string())
        server.quit()


@api_view(['GET'])
def confirm_registration(request, token):
    try:
        token = AccessToken(token)

        if not token['is_confirmation_token']:
            raise TokenError('Некорректный токен подтверждения')

        username = token.get('username')
        user = CustomUser.objects.get(username=username)

        user.is_approved = True
        user.save()
        return redirect('confirmation-success-page')
    except (TokenError, CustomUser.DoesNotExist):
        return Response({'error': 'Неверный токен или пользователь не найден'}, status=status.HTTP_400_BAD_REQUEST)


def confirmation_success(request):
    return render(request, 'confirmation_success.html')


class UserLoginAPIView(GenericAPIView):
    permission_classes = [permissions.AllowAny, ]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data

        # Проверяем, подтвержден ли пользователь
        if not user.is_approved:
            raise AuthenticationFailed('Этот аккаунт еще не подтвержден.')

        token = RefreshToken.for_user(user)
        data = serializer.data
        data["tokens"] = {"refresh": str(token), "access": str(token.access_token)}
        return Response(data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated, IsDirectorOrAdmin])
def approve_user_registration(request, user_id):
    user_to_approve = get_object_or_404(CustomUser, pk=user_id)
    # Проверьте, что запросивший пользователь является директором компании этого пользователя
    if request.user != user_to_approve.company.director:
        return Response({"error": "Вы не имеете права подтвердить этого пользователя"},
                        status=status.HTTP_403_FORBIDDEN)

    user_to_approve.is_approved = True
    user_to_approve.save()
    return Response({"message": "Пользователь подтвержден"}, status=status.HTTP_200_OK)


class CompanyInfo(ListAPIView):
    permission_classes = [permissions.AllowAny, ]
    queryset = Company.objects.all()
    serializer_class = CompanyTitleSerializers


class JobInfo(ListAPIView):
    permission_classes = [permissions.AllowAny, ]
    queryset = Job_title.objects.all()
    serializer_class = JobSerializers


class LogoutView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        try:
            refresh_token = self.get_refresh_token_from_header(request)
            if refresh_token:
                token = RefreshToken(refresh_token)
                token.blacklist()
                return Response(status=status.HTTP_205_RESET_CONTENT)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def get_refresh_token_from_header(self, request):
        authorization_header = request.META.get("HTTP_AUTHORIZATION")
        print(authorization_header)
        if authorization_header:
            try:
                _, token = authorization_header.split("Bearer ")
                return token
            except ValueError:
                pass
        return None
