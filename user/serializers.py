from django.contrib.auth import authenticate
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.exceptions import ParseError
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.exceptions import AuthenticationFailed

from .models import CustomUser, Role, Job_title, Company


class CompanySerializers(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = "__all__"


class CompanyTitleSerializers(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = ('id', 'title')


class CompanySerializers(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = (
            'title',
        )


class RoleSerializers(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = "__all__"


class JobSerializers(serializers.ModelSerializer):
    class Meta:
        model = Job_title
        fields = "__all__"


class ChangeJobSerializer(serializers.ModelSerializer):
    job = serializers.PrimaryKeyRelatedField(queryset=Job_title.objects.all())

    class Meta:
        model = CustomUser
        fields = ['job']


class UserFullSerializers(serializers.ModelSerializer):
    name_role = RoleSerializers()
    job = JobSerializers()
    company = CompanySerializers()

    class Meta:
        model = CustomUser
        fields = '__all__'


class UserProject(serializers.ModelSerializer):
    name_role = RoleSerializers()
    job = JobSerializers()
    company = CompanySerializers()

    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'name_role', 'job', 'company')


class ListInfoUser(serializers.ModelSerializer):
    email = serializers.EmailField()
    company = CompanyTitleSerializers()
    job = JobSerializers()

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'first_name',
            'last_name',
            'patronymic',
            'phone',
            'email',
            'job',
            'birth_date',
            'company'
        )


class UserSerializers(serializers.ModelSerializer):
    email = serializers.EmailField()
    company = CompanyTitleSerializers()
    job = JobSerializers()
    role = RoleSerializers()

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'patronymic',
            'phone',
            'email',
            'role',
            'job',
            'birth_date',
            'company'
        )


class UserSerializersForCompany(serializers.ModelSerializer):
    email = serializers.EmailField()
    job = JobSerializers()
    role = RoleSerializers()

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'patronymic',
            'phone',
            'email',
            'role',
            'job',
            'birth_date',
        )


class UserPutSerializers(serializers.ModelSerializer):
    email = serializers.EmailField()

    class Meta:
        model = CustomUser
        fields = (
            'first_name',
            'last_name',
            'patronymic',
            'phone',
            'email',
            'birth_date',
        )


class RegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    company_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'username',
            'email',
            'password',
            'company_id'
        )
        extra_kwargs = {"password": {"write_only": True}}

    # def create(self, validated_data):
    #     return CustomUser.objects.create_user(**validated_data)

    def create(self, validated_data):
        company_id = validated_data.pop('company_id', None)
        user = CustomUser.objects.create_user(is_approved=False, **validated_data)
        if company_id:
            user.company_id = company_id
            user.save()
            # Здесь отправьте письмо директору компании
        return user

    def validate_email(self, value):
        email = value.lower()
        if CustomUser.objects.filter(email=email).exists():
            raise ParseError(
                'Пользователь с такой почтой уже зарегистрирован'
            )
        return email

    def validate_password(self, value):
        validate_password(value)
        return value


class UserLoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    email = serializers.CharField()
    password = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = (
            'username',
            'email',
            'password'
        )

    def validate(self, data):
        username = data.get("username")
        email = data.get("email")
        password = data.get("password")

        user = authenticate(username=username, email=email, password=password)
        if user and user.is_active:
            return user
        raise serializers.ValidationError('Неправильные учетные данные')


class CustomUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'email'
        )
