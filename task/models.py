from django.db import models
from user.models import CustomUser, Company
from django.utils import timezone


class Urgency(models.Model):
    title = models.CharField(max_length=20, verbose_name='Срочность', null=False, blank=False)

    class Meta:
        verbose_name_plural = "Типы срочности"
        verbose_name = 'Срочность'

    def __str__(self):
        return self.title


class State(models.Model):
    title = models.CharField(max_length=20, verbose_name='Состояние', null=False, blank=False)

    class Meta:
        verbose_name_plural = 'Состояние задачи'
        verbose_name = 'Состояние'

    def __str__(self):
        return self.title


class Comment(models.Model):
    description = models.CharField(max_length=255, verbose_name='Комментарий', default='')
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Автор комментария', null=True, blank=True)
    create_at = models.DateTimeField(default=timezone.now, verbose_name='Дата создания')
    task = models.ForeignKey('Task', on_delete=models.CASCADE, verbose_name='Задача', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Комментарии'
        verbose_name = 'Комментарий'

    def __str__(self):
        return f'{self.author}, {self.description} | {self.create_at}'


class Task(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название задачи', null=False, blank=False, default='Название задачи')
    content = models.CharField(max_length=255, verbose_name='Описание задачи', null=False, blank=False, default='Описание')
    deadline = models.DateField(null=True, blank=True, verbose_name='Дедлайн')
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Автор задачи', null=True, blank=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE, verbose_name='Проект', null=True, blank=True)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Исполнитель', related_name='tasks', null=True, blank=True)
    state = models.ForeignKey('State', on_delete=models.CASCADE, verbose_name='Состояние задачи', default=1)
    urgency = models.ForeignKey('Urgency', on_delete=models.CASCADE, verbose_name='Срочность', default=2)

    class Meta:
        verbose_name_plural = 'Задачи'
        verbose_name = 'Задача'

    def __str__(self):
        return self.title


class Project(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название проекта', null=False, blank=False)
    description = models.CharField(max_length=255, verbose_name='Описание', null=False, blank=False)
    company = models.ForeignKey(Company, null=True, blank=True, on_delete=models.CASCADE, verbose_name='Компания')
    employees = models.ManyToManyField(CustomUser, verbose_name='Участники проекта')
    create_at = models.DateField(auto_now_add=True, verbose_name='Дата создания')
    date_finish = models.DateField(verbose_name='Дата окончания')

    def get_total_tasks(self):
        return self.task_set.count()

    def get_completed_tasks(self):
        return self.task_set.filter(state__id=4).count()

    def get_overdue_tasks(self):
        return self.task_set.filter(deadline__lt=timezone.now()).exclude(state__id=4).count()

    def get_remaining_tasks(self):
        return self.task_set.exclude(state__id=4).count()

    class Meta:
        verbose_name_plural = 'Проекты'
        verbose_name = 'Проект'

    def __str__(self):
        return f'{self.title} {self.company}'
