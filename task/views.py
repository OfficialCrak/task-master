import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.core.mail import EmailMultiAlternatives
from rest_framework import permissions, status
from rest_framework.exceptions import NotFound
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView, GenericAPIView, \
    get_object_or_404, UpdateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from user.models import CustomUser
from .models import Project, Task, Comment
from user.serializers import UserSerializersForCompany
from .serializers import DetailProjectSerializers, PostProjectSerializers, TaskSerializer, TaskDetailSerializer, \
    GetProjectSerializers, PutDetailProjectSerializers, PutTaskSerializer, PostTaskSerializer, CommentSerializer, \
    GETCommentByTask, PostCommentByTask, GETDetailComment, DummySerializer, ChangeTaskStateSerializer, \
    SendTaskForReviewSerializer, ReportSerializer, SimpleSerializer
from .permissions import IsAuthorOrReadOnly, IsDirectorOrAdmin, IsProjectParticipant, IsProjectManagerOrAdmin, \
    IsTaskProjectParticipant, IsProjectParticipantOrReadOnly, IsCommentTaskProjectParticipant, IsTaskExecutor
from rest_framework.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class DetailProject(RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    lookup_field = 'pk'

    def get_serializer_class(self):
        if self.request.method == 'PUT':
            return PutDetailProjectSerializers
        return DetailProjectSerializers

    def get_permissions(self):
        if self.request.method == 'GET':
            return [IsProjectParticipant()]
        elif self.request.method in ['PUT', 'PATCH', 'DELETE']:
            return [IsProjectManagerOrAdmin()]
        return []

    def perform_update(self, serializer):
        employees_data = self.request.data.get('employees')
        if employees_data:
            serializer.save(employees=employees_data)
        else:
            serializer.save()


class PostProject(ListCreateAPIView):
    queryset = Project.objects.all()
    model = Project

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return GetProjectSerializers
        return PostProjectSerializers

    def get_permissions(self):
        if self.request.method in ['POST', 'PUT', 'DELETE']:
            # Возвращаем экземпляр класса разрешения
            return [IsDirectorOrAdmin()]
        # Если метод не POST, PUT или DELETE, то применяем IsAuthenticated
        return [IsAuthenticated()]

    def get_queryset(self):
        if self.request.user.is_staff:
            return Project.objects.all()
        return Project.objects.filter(employees=self.request.user)

    def perform_create(self, serializer):
        current_datetime = timezone.now()
        date_finish = serializer.validated_data.get('date_finish')

        if date_finish and date_finish <= current_datetime.date():
            raise ValidationError(_('Дата окончания проекта не может быть раньше текущей даты и времени.'))
        serializer.save(company=self.request.user.company)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class AddUserToProjectView(GenericAPIView):
    permission_classes = [IsDirectorOrAdmin]
    serializer_class = DummySerializer
    queryset = Project.objects.all()

    def post(self, request, *args, **kwargs):
        project = self.get_object()
        user_id = request.data.get('user_id')
        user = get_object_or_404(CustomUser, pk=user_id)

        if not request.user.is_staff and request.user.role_id != 4:
            return Response({"error": "Недостаточно прав для добавления пользователя"}, status=403)

        project.employees.add(user)
        return Response({"message": "Пользователь добавлен в проект"}, status=status.HTTP_200_OK)


class RemoveUserFromProjectView(GenericAPIView):
    permission_classes = [IsDirectorOrAdmin]
    serializer_class = DummySerializer
    queryset = Project.objects.all()

    def post(self, request, *args, **kwargs):
        project = self.get_object()
        user_id = request.data.get('user_id')
        user = get_object_or_404(CustomUser, pk=user_id)

        if not request.user.is_staff and request.user.role_id != 4:
            return Response({"error": "Недостаточно прав для удаления пользователя"}, status=403)

        project.employees.remove(user)
        return Response({"message": "Пользователь удален из проекта"}, status=status.HTTP_200_OK)


class CompanyUserListView(ListAPIView):
    serializer_class = UserSerializersForCompany
    permission_classes = [IsDirectorOrAdmin]

    def get_queryset(self):
        return CustomUser.objects.filter(company=self.request.user.company)


class ProjectUsersListView(ListAPIView):
    serializer_class = UserSerializersForCompany
    permission_classes = [IsDirectorOrAdmin]

    def get_queryset(self):
        project_id = self.kwargs.get('pk')
        return CustomUser.objects.filter(project__id=project_id)


class TaskListCreateView(ListCreateAPIView):

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return PostTaskSerializer
        return TaskSerializer

    def get_permissions(self):
        if self.request.method == 'POST':
            return [IsProjectParticipantOrReadOnly()]
        return [IsProjectParticipant()]

    def get_queryset(self):
        project_id = self.kwargs['project_id']
        queryset = Task.objects.filter(project_id=project_id)
        return queryset

    def perform_create(self, serializer):
        project_id = self.kwargs.get('project_id')
        try:
            project = Project.objects.get(pk=project_id)
        except Project.DoesNotExist:
            raise NotFound('Проект не найден')
        serializer.save(author=self.request.user, project=project)


class TaskDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskDetailSerializer
    permission_classes = [IsAuthorOrReadOnly, IsTaskProjectParticipant]

    def get_queryset(self):
        project_id = self.kwargs['project_id']
        queryset = Task.objects.filter(project_id=project_id)
        return queryset

    @swagger_auto_schema(request_body=PutTaskSerializer)
    def put(self, request, *args, **kwargs):
        self.serializer_class = PutTaskSerializer
        return self.update(request, *args, **kwargs)


class CommentList(ListAPIView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        queryset = Comment.objects.all()
        return queryset


class CommentByTask(ListCreateAPIView):

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return [IsCommentTaskProjectParticipant()]
        return [IsProjectParticipantOrReadOnly()]

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return PostCommentByTask
        return GETCommentByTask

    def get_queryset(self):
        task_id = self.kwargs['task_id']
        queryset = Comment.objects.filter(task_id=task_id)
        return queryset

    def perform_create(self, serializer):
        task_id = self.kwargs.get('task_id')
        try:
            task = Task.objects.get(pk=task_id)
        except Project.DoesNotExist:
            raise NotFound('Задача не найдена')
        serializer.save(author=self.request.user, task=task)


class CommentDetail(RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = GETDetailComment
    permission_classes = [IsAuthorOrReadOnly, IsCommentTaskProjectParticipant]

    def get_queryset(self):
        task_id = self.kwargs['task_id']
        queryset = Comment.objects.filter(task_id=task_id)
        return queryset

    @swagger_auto_schema(request_body=PostCommentByTask)
    def put(self, request, *args, **kwargs):
        self.serializer_class = PostCommentByTask
        return self.update(request, *args, **kwargs)


class ChangeTaskStateView(UpdateAPIView):
    queryset = Task.objects.all()
    serializer_class = ChangeTaskStateSerializer
    permission_classes = [IsAuthorOrReadOnly, IsTaskProjectParticipant]

    def perform_update(self, serializer):
        serializer.save()


class SendTaskForReviewView(GenericAPIView):
    queryset = Task.objects.all()
    serializer_class = SendTaskForReviewSerializer
    permission_classes = [IsTaskExecutor]

    def post(self, request, *args, **kwargs):
        task = self.get_object()
        serializer = self.get_serializer(task, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"message": "Состояние задачи обновлено."})


class ProjectReportView(RetrieveAPIView):
    queryset = Project.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [IsProjectParticipant]

    def get_serializer_context(self):
        context = super().get_serializer_context()
        project = self.get_object()
        context.update({
            'total_tasks': project.get_total_tasks(),
            'completed_tasks': project.get_completed_tasks(),
            'overdue_tasks': project.get_overdue_tasks(),
            'remaining_tasks': project.get_remaining_tasks(),
        })
        return context


class SendProjectReport(GenericAPIView):
    lookup_field = 'pk'
    serializer_class = DummySerializer

    def post(self, request, pk):
        project = Project.objects.get(pk=pk)
        serializer_context = {
            'total_tasks': project.get_total_tasks(),
            'completed_tasks': project.get_completed_tasks(),
            'overdue_tasks': project.get_overdue_tasks(),
            'remaining_tasks': project.get_remaining_tasks(),
        }
        report_serializer = ReportSerializer(project, context=serializer_context)
        report_data = report_serializer.data

        # Получаем email из запроса
        user_email = request.user.email
        subject = f"Отчет по проекту: {project.title}"
        from_email = "chirjeveg@gmail.com"

        html_content = f"""
        <html>
            <body>
                <h1>Отчет по проекту: {project.title}</h1>
                <p><strong>Компания:</strong> {report_data.get('company')}</p>
                <p><strong>Описание проекта:</strong> {report_data.get('description')}</p>
                <p><strong>Время создания:</strong> {report_data.get('create_at')}</p>
                <p><strong>Время окончания:</strong> {report_data.get('date_finish')}</p>
                <p><strong>Участники проекта:</strong> {', '.join([f"{emp['first_name']} {emp['last_name']} ({emp['job_title']})" for emp in report_data.get('employees', [])])}</p>
                <p><strong>Общее количество задач:</strong> {report_data.get('total_tasks')}</p>
                <p><strong>Выполненные задачи:</strong> {report_data.get('completed_tasks')}</p>
                <p><strong>Просроченные задачи:</strong> {report_data.get('overdue_tasks')}</p>
                <p><strong>Осталось выполнить задач:</strong> {report_data.get('remaining_tasks')}</p>
            </body>
        </html>
        """

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = from_email
        msg['To'] = user_email

        part2 = MIMEText(html_content, 'html')
        msg.attach(part2)

        with smtplib.SMTP('smtp.gmail.com', 587) as server:
            server.starttls()
            server.login(from_email, 'zokhngohtxydrotq')
            server.sendmail(from_email, user_email, msg.as_string())

        return Response({"message": "Отчет отправлен"}, status=status.HTTP_200_OK)
