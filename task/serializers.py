from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Task, Project, Urgency, State, Comment
from user.serializers import CompanyTitleSerializers, CompanySerializers
from user.models import CustomUser


class StateSerializers(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = '__all__'


class UrgencySerializers(serializers.ModelSerializer):

    class Meta:
        model = Urgency
        fields = '__all__'


class ProjectSerializers(serializers.ModelSerializer):
    Company = CompanyTitleSerializers()

    class Meta:
        model = Project
        fields = ('id', 'title', 'description', 'company', 'create_at', 'date_finish')


class PostProjectSerializers(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = (
            'title',
            'description',
            'employees',
            'date_finish'
        )


class GetProjectSerializers(serializers.ModelSerializer):
    company = CompanyTitleSerializers()
    employees = serializers.StringRelatedField(many=True)

    class Meta:
        model = Project
        fields = (
            'id',
            'title',
            'description',
            'company',
            'employees',
            'create_at',
            'date_finish'
        )


class PutDetailProjectSerializers(serializers.ModelSerializer):
    employees = serializers.PrimaryKeyRelatedField(many=True, queryset=CustomUser.objects.all())

    class Meta:
        model = Project
        fields = (
            'title',
            'description',
            'employees',
            'date_finish'
        )


class EmployeesSerializer(serializers.ModelSerializer):
    job = serializers.CharField(source='job.title', read_only=True)

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'job',
        )


class DetailProjectSerializers(serializers.ModelSerializer):
    company = CompanyTitleSerializers()
    employees = EmployeesSerializer(many=True, read_only=True)

    class Meta:
        model = Project
        fields = ('id', 'title', 'description', 'company', 'employees', 'create_at', 'date_finish')


class TaskSerializer(serializers.ModelSerializer):
    state = serializers.CharField(source="state.title", read_only=True)
    urgency = serializers.CharField(source="urgency.title", read_only=True)

    class Meta:
        model = Task
        fields = (
            'id',
            'title',
            'content',
            'state',
            'urgency'
        )


class PostTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = (
            'title',
            'content',
            'deadline',
            'user',
            'state',
            'urgency'
        )


class CommentSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        user = obj.author
        return {
            "id": user.id,
            "login": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "job": user.job.title
        }

    class Meta:
        model = Comment
        fields = (
            'id',
            'description',
            'author',
            'create_at',
            'task',
        )


class GETCommentByTask(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        user = obj.author
        return {
            "id": user.id,
            "login": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "job": user.job.title
        }

    class Meta:
        model = Comment
        fields = (
            'id',
            'description',
            'author',
            'create_at',
        )


class TaskDetailSerializer(serializers.ModelSerializer):
    author = serializers.CharField(source="author.username", read_only=True)
    user = serializers.CharField(source="user.username", read_only=True)
    state = serializers.CharField(source="state.title", read_only=True)
    urgency = serializers.CharField(source="urgency.title", read_only=True)

    comments = GETCommentByTask(many=True, read_only=True)

    class Meta:
        model = Task
        fields = (
            'id',
            'title',
            'content',
            'deadline',
            'author',
            'user',
            'state',
            'urgency',
            'comments',
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['comments'] = CommentSerializer(instance.comment_set.all(), many=True).data
        return representation


class PutTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = (
            'title',
            'content',
            'deadline',
            'user',
            'urgency'
        )


class PostCommentByTask(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = (
            'description',
        )


class GETDetailComment(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        user = obj.author
        return {
            "id": user.id,
            "login": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "job": user.job.title
        }

    class Meta:
        model = Comment
        fields = (
            'description',
            'author',
            'create_at',
        )


class DummySerializer(serializers.Serializer):
    pass


class ChangeTaskStateSerializer(serializers.ModelSerializer):
    state = serializers.PrimaryKeyRelatedField(queryset=State.objects.filter(id__in=[1, 2, 4]))

    class Meta:
        model = Task
        fields = ['state']


class SendTaskForReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['state']

    def update(self, instance, validated_data):
        new_state_id = validated_data.get('state').id
        if instance.state_id == 1 and new_state_id == 2:
            instance.state_id = new_state_id
        elif instance.state_id == 2 and new_state_id == 3:
            instance.state_id = new_state_id
        else:
            raise ValidationError("Недопустимое изменение состояния задачи.")
        instance.save()
        return instance


class ReportSerializer(serializers.ModelSerializer):
    total_tasks = serializers.SerializerMethodField()
    completed_tasks = serializers.SerializerMethodField()
    overdue_tasks = serializers.SerializerMethodField()
    remaining_tasks = serializers.SerializerMethodField()
    company = serializers.SerializerMethodField()
    employees = serializers.SerializerMethodField()

    class Meta:
        model = Project
        fields = (
            'company',
            'title',
            'description',
            'create_at',
            'date_finish',
            'employees',
            'total_tasks',
            'completed_tasks',
            'overdue_tasks',
            'remaining_tasks',
        )

    def get_total_tasks(self, obj):
        return obj.get_total_tasks()

    def get_completed_tasks(self, obj):
        return obj.get_completed_tasks()

    def get_overdue_tasks(self, obj):
        return obj.get_overdue_tasks()

    def get_remaining_tasks(self, obj):
        return obj.get_remaining_tasks()

    def get_company(self, obj):
        return obj.company.title if obj.company else None

    def get_employees(self, obj):
        return [
            {
                "first_name": employee.first_name,
                "last_name": employee.last_name,
                "job_title": employee.job.title if employee.job else None
            }
            for employee in obj.employees.all()
        ]


class SimpleSerializer(serializers.Serializer):
    email = serializers.EmailField()
