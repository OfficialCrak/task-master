from django.urls import path
from .views import DetailProject, PostProject, TaskListCreateView, TaskDetailView, CommentList, CommentByTask, \
    CommentDetail, AddUserToProjectView, RemoveUserFromProjectView, ProjectUsersListView, CompanyUserListView, \
    ChangeTaskStateView, SendTaskForReviewView, ProjectReportView, SendProjectReport

urlpatterns = [
    # post views
    path('', PostProject.as_view(), name='api_project_list'),
    path('<int:pk>/', DetailProject.as_view()),
    path('<int:project_id>/tasks/', TaskListCreateView.as_view()),
    path('<int:project_id>/tasks/<int:pk>/', TaskDetailView.as_view()),
    path('comment/list/', CommentList.as_view()),
    path('<int:project_id>/tasks/<int:task_id>/comments/', CommentByTask.as_view()),
    path('<int:project_id>/tasks/<int:task_id>/comments/<int:pk>/', CommentDetail.as_view()),
    path('<int:pk>/add-user/', AddUserToProjectView.as_view()),
    path('<int:pk>/remove-user/', RemoveUserFromProjectView.as_view()),
    path('company-users/', CompanyUserListView.as_view()),
    path('<int:pk>/users/', ProjectUsersListView.as_view()),
    path('<int:project_id>/tasks/<int:pk>/change-state/', ChangeTaskStateView.as_view()),
    path('<int:project_id>/tasks/<int:pk>/send-for-review/', SendTaskForReviewView.as_view()),
    path('<int:pk>/report/', ProjectReportView.as_view(), name='project-report'),
    path('<int:pk>/send-report/', SendProjectReport.as_view(), name='send-project-report'),
]
