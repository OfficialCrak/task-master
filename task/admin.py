from django.contrib import admin
from .models import Urgency, State, Project, Task, Comment
from user.models import CustomUser


ProjectEmployees = Project.employees.through


class ProjectEmployeesAdmin(admin.ModelAdmin):
    model = ProjectEmployees
    list_display = ['project_id', 'project', 'customuser_id', 'customuser', 'get_job']

    def get_job(self, obj):
        return obj.customuser.job
    get_job.short_description = 'Должность'

    search_fields = ['project__title', 'customuser__username']


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title', 'company', 'create_at', 'date_finish')
    search_fields = ('title', 'company', 'create_at', 'date_finish')


class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'deadline', 'author', 'user', 'project', 'state', 'urgency')
    search_fields = ('title', 'user')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('description', 'author', 'get_job', 'get_company', 'create_at', 'task')

    def get_job(self, obj):
        return obj.author.job if obj.author else None
    get_job.short_description = 'Должность'

    def get_company(self, obj):
        return obj.author.company
    get_company.short_description = "Компания"

    search_fields = ('author__username', )


admin.site.register(Urgency)
admin.site.register(State)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(ProjectEmployees, ProjectEmployeesAdmin)
