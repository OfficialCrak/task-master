from rest_framework import permissions
from .models import Project, Task, Comment


class IsAuthorOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_authenticated is True

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.author == request.user or request.user.is_staff or request.user.is_superuser


class IsDirectorOrAdmin(permissions.BasePermission):
    """
    Пользователь с ролью "Директор" или "Администратор" (по role_id) имеет доступ к созданию проектов.
    """

    def has_permission(self, request, view):
        user = request.user
        return user.is_authenticated and (user.role_id == 4 or user.is_staff)


class IsProjectParticipant(permissions.BasePermission):
    """
    Пользователь, участвующий в проекте, имеет доступ к просмотру проекта.
    """

    def has_object_permission(self, request, view, obj):
        user = request.user
        return user in obj.employees.all()


class IsProjectManagerOrAdmin(permissions.BasePermission):
    """
    Только директор или администратор имеют доступ к обновлению проекта.
    """

    def has_object_permission(self, request, view, obj):
        user = request.user
        return user.is_authenticated and (user.role_id == 4 or user.is_staff)


class IsTaskProjectParticipant(permissions.BasePermission):
    """
    Пользователь, участвующий в проекте, связанном с задачей, имеет доступ к просмотру этой задачи.
    """

    def has_object_permission(self, request, view, obj):
        # Предполагается, что у задачи есть атрибут 'project', который является ссылкой на проект
        project = obj.project
        return request.user in project.employees.all()


class IsProjectParticipantOrReadOnly(permissions.BasePermission):
    """
    Разрешение, позволяющее участникам проекта создавать и просматривать задачи.
    Другим пользователям разрешено только чтение.
    """

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        project_id = view.kwargs.get('project_id')
        try:
            project = Project.objects.get(pk=project_id)
        except Project.DoesNotExist:
            return False

        return request.user in project.employees.all()


class IsCommentTaskProjectParticipant(permissions.BasePermission):
    """
    Пользователь, участвующий в проекте, связанном с задачей комментария, имеет доступ к просмотру этого комментария.
    """

    def has_permission(self, request, view):
        if request.user.is_staff or request.user.is_superuser:
            return True

        task_id = view.kwargs.get('task_id')
        try:
            task = Task.objects.get(pk=task_id)
        except Task.DoesNotExist:
            return False

        return request.user in task.project.employees.all()


class IsTaskExecutor(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
