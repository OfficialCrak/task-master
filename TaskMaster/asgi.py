"""
ASGI config for TaskMaster project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/asgi/
"""

import os

from channels.routing import ProtocolTypeRouter, URLRouter
from channels_auth_token_middlewares.middleware import QueryStringSimpleJWTAuthTokenMiddleware
from django.urls import path
from django.core.asgi import get_asgi_application
from chat.consumers import ChatConsumer

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'TaskMaster.settings')

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": QueryStringSimpleJWTAuthTokenMiddleware(
            URLRouter([
                path("ws/chat/<int:room_id>/", ChatConsumer.as_asgi()),
            ]),
    )
})
