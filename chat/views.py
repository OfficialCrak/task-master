# views.py

from rest_framework import generics
from .models import ChatRoom, Message
from .serializers import ChatRoomSerializer, MessageSerializer, MessageContentSerializer, DetailChatMessage


class CreateChatRoomView(generics.CreateAPIView):
    queryset = ChatRoom.objects.all()
    serializer_class = ChatRoomSerializer


class ChatRoomListView(generics.ListAPIView):
    queryset = ChatRoom.objects.all()
    serializer_class = ChatRoomSerializer


class ChatRoomDetailView(generics.ListAPIView):
    serializer_class = DetailChatMessage
    lookup_field = 'room_id'

    def get_queryset(self):
        room_id = self.kwargs['room_id']
        queryset = Message.objects.filter(room_id=room_id)
        return queryset


class RoomDetailMessage(generics.RetrieveAPIView):
    serializer_class = DetailChatMessage

    def get_queryset(self):
        room_id = self.kwargs['room_id']
        queryset = Message.objects.filter(room_id=room_id)
        return queryset


class SendMessageView(generics.CreateAPIView):
    serializer_class = MessageContentSerializer

    def perform_create(self, serializer):
        room_id = self.kwargs['room_id']
        serializer.save(room_id=room_id, sender=self.request.user)

