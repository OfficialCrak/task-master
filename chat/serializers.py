from rest_framework import serializers
from .models import ChatRoom, Message
from user.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'first_name', 'last_name')


class MessageSerializer(serializers.ModelSerializer):
    sender = UserSerializer()

    class Meta:
        model = Message
        fields = ('id', 'room', 'sender', 'content', 'timestamp')


# class RoomSerializer(serializers.ModelSerializer):
#     last_message = serializers.SerializerMethodField()
#     messages = MessageSerializer(many=True, read_only=True)
#
#     class Meta:
#         model = ChatRoom
#         fields = ('pk', 'name', 'host', 'messages', 'current_users', 'last_message')
#         depth = 1
#         read_only_fields = ('messages', 'last_message')
#
#     def get_last_message(self, obj: ChatRoom):
#         return MessageSerializer(obj.messages.order_by('created_at').last()).data
class DetailChatMessage(serializers.ModelSerializer):
    sender = UserSerializer()

    class Meta:
        model = Message
        fields = ('id', 'sender', 'content', 'timestamp')


class ChatRoomSerializer(serializers.ModelSerializer):
    participants = UserSerializer(many=True)
    last_message = serializers.SerializerMethodField()
    messages = DetailChatMessage(many=True, read_only=True)

    class Meta:
        model = ChatRoom
        # fields = ('id', 'participants')
        fields = '__all__'
        depth = 1
        read_only_fields = ['messages', 'last_message']

    def get_last_message(self, obj: ChatRoom):
        return DetailChatMessage(obj.messages.order_by('timestamp').last()).data


class MessageContentSerializer(serializers.ModelSerializer):
    content = serializers.CharField()

    class Meta:
        model = Message
        exclude = []
        depth = 1

    def create(self, validated_data):
        return Message.objects.create(**validated_data)

