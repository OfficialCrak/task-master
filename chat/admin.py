from django.contrib import admin
from .models import ChatRoom, Message


class ChatRoomAdmin(admin.ModelAdmin):
    list_display = ('id', )


class MessageAdmin(admin.ModelAdmin):
    list_display = ('room', 'sender', 'content', 'timestamp')


admin.site.register(ChatRoom, ChatRoomAdmin)
admin.site.register(Message, MessageAdmin)
