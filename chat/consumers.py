import json
from channels.generic.websocket import AsyncWebsocketConsumer
from .models import ChatRoom, Message
from user.models import CustomUser
from channels.db import database_sync_to_async


def get_chat_room(room_id):
    try:
        return ChatRoom.objects.get(id=room_id)
    except ChatRoom.DoesNotExist:
        # Обработка случая, если чат с указанным room_id не найден
        return None


def get_user_by_id(user_id):
    try:
        return CustomUser.objects.get(id=user_id)
    except CustomUser.DoesNotExist:
        # Обработка случая, если пользователь с указанным user_id не найден
        return None


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        room_id = self.scope['url_route']['kwargs']['room_id']
        self.room_group_name = f'chat_{room_id}'

        # Присоединение к комнате чата
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Отсоединение от комнаты чата
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        # sender = self.scope['user']
        # Принимаем данные от веб-сокета и отправляем их обратно в комнату чата
        message_data = json.loads(text_data)
        sender_id = message_data.get('senderId')
        message_content = message_data['message']
        room_id = self.scope['url_route']['kwargs']['room_id']

        room = await database_sync_to_async(get_chat_room)(room_id)
        sender = await database_sync_to_async(get_user_by_id)(sender_id)

        message = Message(room=room, sender=sender, content=message_content)
        await database_sync_to_async(message.save)()

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat.message',
                'message': message_content,
                'senderId': sender_id,
            }
        )

    async def chat_message(self, event):
        # Отправляем сообщение обратно в веб-сокет клиентам
        message = event['message']
        sender_id = event['senderId']

        sender = await database_sync_to_async(get_user_by_id)(sender_id)

        if sender:
            sender_data = {
                'id': sender.id,
                'username': sender.username,
            }

            if sender.last_name:
                sender_data['last_name'] = sender.last_name
            if sender.first_name:
                sender_data['first_name'] = sender.first_name

            await self.send(text_data=json.dumps({
                'message': message,
                'sender': sender_data,
            }))

        else:
            # Если пользователь не найден, отправляем только сообщение без данных о пользователе
            await self.send(text_data=json.dumps({
                'message': message,
                'senderId': sender_id,
            }))

