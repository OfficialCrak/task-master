from django.urls import path
from . import views

urlpatterns = [
    # URL для создания новой комнаты чата
    path('create/', views.CreateChatRoomView.as_view(), name='create-chat-room'),

    # URL для получения списка всех чатов
    path('', views.ChatRoomListView.as_view(), name='chat-room-list'),

    # URL для работы с конкретным чатом
    path('<int:room_id>/', views.ChatRoomDetailView.as_view(), name='chat-room-detail'),

    # URL для отправки сообщения в чат
    path('<int:room_id>/send/', views.SendMessageView.as_view(), name='send-message'),
    path('<int:room_id>/messages/<int:pk>/', views.RoomDetailMessage.as_view(), name='message-detail'),
]
