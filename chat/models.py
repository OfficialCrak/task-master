from django.db import models
from user.models import CustomUser


# class ChatRoom(models.Model):
#     name = models.CharField(max_length=255, unique=True, verbose_name='Название комнаты')
#     host = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='rooms', null=True, blank=True)
#     current_users = models.ManyToManyField(CustomUser, related_name='current_rooms')
#
#     class Meta:
#         verbose_name_plural = 'Комнаты'
#         verbose_name = 'Комната'
#
#     def __str__(self):
#         return f"Room({self.name} {self.host})"
#
#
# class Message(models.Model):
#     room = models.ForeignKey(ChatRoom, on_delete=models.CASCADE, related_name='messages', verbose_name='Комната')
#     user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='current_users', null=True, blank=True)
#     text = models.TextField(verbose_name='Сообщение')
#     created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время отправки')
#
#     class Meta:
#         verbose_name_plural = 'Сообщения'
#         verbose_name = 'Сообщение'
#
#     def __str__(self):
#         return f'Message({self.user} {self.room})'


class ChatRoom(models.Model):
    participants = models.ManyToManyField(CustomUser, related_name='chat_rooms')

    class Meta:
        verbose_name_plural = 'Комнаты'
        verbose_name = 'Комната'

    def __str__(self):
        return f"{self.id}"


class Message(models.Model):
    room = models.ForeignKey(ChatRoom, on_delete=models.CASCADE, related_name='messages')
    sender = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True, blank=True)
    content = models.TextField(default='')
    timestamp = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        verbose_name_plural = 'Сообщения'
        verbose_name = 'Сообщение'

    def __str__(self):
        return f"{self.room} {self.sender} {self.content}"
